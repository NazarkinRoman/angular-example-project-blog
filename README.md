# AngularExampleProject

Проект представляет собой простой блог с асинхронной подгрузкой 
постов из файла `app-data.json`, поэтапной подгрузкой постов по 
мере прокручивания страницы (infinity scrolling), сохранением 
состояния и возможностью добавлять понравившиеся посты в избранное.

## Что использовал

Из сторонних зависимостей использованы только следующие:
- `bootstrap` как дизайн-систему
- полифилл для `intersection-observer`

Для реализации сервиса обмена данными (`DataProviderService`) применил `RxJS`. 
Таким образом, сервис можно переписать на реальную работу с внешним API без 
изменений в логике остальных частей приложения. 

Для работы со списком избранного 
реализовал простой сервис `FavoritesStorageService` также использующий `RxJS` 
и `SessionStorage` под капотом.

## Как делал

Некоторые вещи в данном проекте можно было бы заменить готовыми решениями, но чтобы 
работа была показательной я реализовал их самостоятельно. Так, например, 
можно было бы заменить:

- `ExcerptPipe` - удаляет HTML теги из входной строки и укорачивает ее до необходимого
числа символов избегая разбивания слов на части
- `InViewportDirective` - обертка над `IntersectionObserver`
- `LazyLoadImageComponent` - компонент для вставки картинки с функцией 
ленивой загрузкой и прелоадером
- `PostDetailsModalComponent` - компонент модального окна с полной информацией о 
выбранном посте
