/**
 * Сущность категории из блога
 */
export interface PostCategory {
  id: number;
  title: string;
  postCount?: number;
}

/**
 * Сущность поста из блога
 */
export interface SinglePost {
  id: number;
  title: string;
  body: string;
  imageThumb: string;
  imageFull: string;
  categoryId: number;
  category: PostCategory;
}

/**
 * Сщуность списка постов с указанием номера страницы
 */
export interface PostsList {
  page: number;
  posts: SinglePost[];
  isLast: boolean;
}

/**
 * Сущность, описывающая формат данных в файле 'app-data.json'
 */
export interface DataFileContents {
  posts: SinglePost[];
  categories: PostCategory[];
}

/**
 * Сущность, описывающая список избранных постов
 */
export type FavoritePostsList = number[];

/**
 * Временная заглушка, пока IntersectionObserver недоступен внутри Window
 * @link https://github.com/Microsoft/TypeScript/issues/16255
 */
declare global {
  interface Window {
    IntersectionObserver: typeof IntersectionObserver;
    IntersectionObserverEntry: typeof IntersectionObserverEntry;
  }
}
