import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { combineLatest, from, Observable } from 'rxjs';
import { count, filter, first, map, mergeMap, pluck, publishReplay, refCount, retry, skip, take, toArray } from 'rxjs/operators';
import { DataFileContents, PostCategory, PostsList, SinglePost } from './typings';

@Injectable({ providedIn: 'root' })
export class DataProviderService {
  /**
   * Количество постов на одной странице
   */
  private postsPerPage = 10;

  /**
   * Расположение файла с данными
   */
  private datafileUrl = '/app-data.json';

  /**
   * Observable с кешированным значением содержимого файла с данными
   */
  private datafileObservable: Observable<DataFileContents>;

  /**
   * Конструктор
   */
  constructor(private http: HttpClient) {
  }

  /**
   * Возвращает поток с содержимым файла данных
   */
  fetchDatafile(): Observable<DataFileContents> {
    if (!this.datafileObservable) {
      this.datafileObservable = this.http.get<DataFileContents>(this.datafileUrl).pipe(
        retry(3),
        publishReplay(1),
        refCount()
      );
    }

    return this.datafileObservable;
  }

  /**
   * Возвращает поток со всеми постами из файла данных
   */
  fetchAllPosts(): Observable<SinglePost> {
    const posts$ = this.fetchDatafile().pipe(
      pluck('posts'),
      mergeMap(posts => from(posts))
    );

    const setCategory = post => this.fetchAllCategories().pipe(
      first(category => category.id === post.categoryId),
      map(category => ({ ...post, category }))
    );

    return posts$.pipe(mergeMap(setCategory));
  }

  /**
   * Возвращает поток с массивом постов по номеру страницы
   */
  fetchPostsByPage(page: number): Observable<PostsList> {
    const remainingPosts$ = this.fetchAllPosts().pipe(
      skip((page - 1) * this.postsPerPage)
    );

    return combineLatest(
      remainingPosts$.pipe(count()),
      remainingPosts$.pipe(take(this.postsPerPage), toArray())
    ).pipe(
      map(([remainingPostsCount, posts]) => ({ page, posts, isLast: remainingPostsCount <= this.postsPerPage }))
    );
  }

  /**
   * Возвращает поток, содержащий все посты из указанной категории
   */
  fetchAllPostsByCategory(categoryId: number): Observable<SinglePost> {
    return this.fetchAllPosts().pipe(filter(post => post.category.id === categoryId));
  }

  /**
   * Возвращает поток с массивом постов по номеру страницы и указанной категории
   */
  fetchPostsByCategoryAndPage(categoryId: number, page: number): Observable<PostsList> {
    const remainingPosts$ = this.fetchAllPostsByCategory(categoryId).pipe(
      skip((page - 1) * this.postsPerPage)
    );

    return combineLatest(
      remainingPosts$.pipe(count()),
      remainingPosts$.pipe(take(this.postsPerPage), toArray())
    ).pipe(
      map(([remainingPostsCount, posts]) => ({ page, posts, isLast: remainingPostsCount <= this.postsPerPage }))
    );
  }

  /**
   * Возвращает поток с категориями из файла данных.
   * Дополнительно для каждой категории просчитывается поле postCount
   */
  fetchAllCategories(): Observable<PostCategory> {
    return this.fetchDatafile().pipe(
      pluck('categories'),
      mergeMap(categories => from(categories))
    );
  }
}
