import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { FavoritePostsList } from './typings';

@Injectable({ providedIn: 'root' })
export class FavoritesStorageService {
  protected storageKey = 'app_favorite_posts';
  protected cachedState: FavoritePostsList;
  protected subject: BehaviorSubject<FavoritePostsList>;

  /**
   * Конструктор
   */
  constructor() {
    const savedState = window.sessionStorage.getItem(this.storageKey);
    this.cachedState = savedState ? JSON.parse(savedState) : [];
    this.subject = new BehaviorSubject(this.cachedState);
  }

  /**
   * Добавление поста в избранное по ID
   */
  add(postId: number): void {
    if (this.cachedState.indexOf(postId) === -1) {
      this.cachedState.push(postId);
      this.updateState();
    }
  }

  /**
   * Удаление поста из избранного по ID
   */
  remove(postToRemove: number): void {
    if (this.cachedState.indexOf(postToRemove) !== -1) {
      this.cachedState = this.cachedState.filter(favId => favId !== postToRemove);
      this.updateState();
    }
  }

  /**
   * Возвращает поток данных, содержащий избранные посты пользователя
   */
  list(): Observable<number[]> {
    return this.subject;
  }

  /**
   * Обновляет значение в localStorage и уведомляет всех подписчиков об изменении
   */
  updateState(): void {
    window.sessionStorage.setItem(this.storageKey, JSON.stringify(this.cachedState));

    if (this.subject) {
      this.subject.next(this.cachedState);
    }
  }
}
