import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CategoriesListComponent } from './categories-list/categories-list.component';
import { PostsListComponent } from './posts-list/posts-list.component';
import { HttpClientModule } from '@angular/common/http';
import { ExcerptPipe } from './excerpt.pipe';
import { InViewportDirective } from './in-viewport.directive';
import { SinglePostComponent } from './single-post/single-post.component';
import { FavoritesListComponent } from './favorites-list/favorites-list.component';
import { PostDetailsModalComponent } from './post-details-modal/post-details-modal.component';
import { LazyLoadImageComponent } from './lazy-load-image/lazy-load-image.component';

@NgModule({
  declarations: [
    AppComponent,
    CategoriesListComponent,
    PostsListComponent,
    ExcerptPipe,
    InViewportDirective,
    SinglePostComponent,
    FavoritesListComponent,
    PostDetailsModalComponent,
    LazyLoadImageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
