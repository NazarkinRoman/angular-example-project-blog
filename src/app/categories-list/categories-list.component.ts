import { Component, OnDestroy, OnInit } from '@angular/core';
import { DataProviderService } from '../data-provider.service';
import { PostCategory } from '../typings';
import { Observable, Subscription, zip } from 'rxjs';
import { count, filter, map, mergeMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent implements OnInit, OnDestroy {
  /**
   * Список категорий из файла данных
   */
  categories: PostCategory[] = [];

  /**
   * Общее число постов в файле данных
   */
  totalPostCount: number;

  /**
   * Массив подписок на потоки данных из провайдера
   */
  subscriptions: Subscription[] = [];

  /**
   * Конструктор
   */
  constructor(private dataProvider: DataProviderService) {
  }

  /**
   * Создаем подписки на необходимые события из провайдера данных
   */
  ngOnInit() {
    const processCategory = category => this.dataProvider.fetchAllPostsByCategory(category.id).pipe(
      count(), map(postCount => ({ ...category, postCount }))
    );

    this.subscriptions.push(this.dataProvider.fetchAllCategories().pipe(mergeMap(processCategory))
      .subscribe(categories => this.categories.push(categories)));

    this.subscriptions.push(this.dataProvider.fetchAllPosts().pipe(count())
      .subscribe(totalPostCount => this.totalPostCount = totalPostCount));
  }

  /**
   * Отписываемся от событий при уничтожении компонента
   */
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
