import { Component, OnDestroy, OnInit } from '@angular/core';
import { animate, query, stagger, style, transition, trigger } from '@angular/animations';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { combineLatest, Subscription } from 'rxjs';
import { distinctUntilChanged, map, pluck } from 'rxjs/operators';
import { DataProviderService } from '../data-provider.service';
import { PostsList } from '../typings';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss'],
  animations: [
    trigger('staggeredReveal', [
      transition('* <=> *', [
        query(':enter', [
            style({ opacity: 0, transform: 'translateX(-50px)' }),
            stagger('100ms', animate('500ms ease-out'))
          ], { optional: true }
        )
      ])
    ])
  ]
})
export class PostsListComponent implements OnInit, OnDestroy {
  /**
   * Выбранная категория
   */
  category: number | string = 'all';

  /**
   * Текущая страница пагинации
   */
  page = 1;

  /**
   * Индикатор процесса загрузки
   */
  isBusy = true;

  /**
   * Список загруженных и отрисованных страниц с постами
   */
  loadedPostsLists: PostsList[] = [];

  /**
   * Массив подписок на Observables
   */
  subscriptions: Subscription[] = [];

  /**
   * Конструктор
   */
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataProviderService) {
  }

  /**
   * Подписываемся на изменения в роуте
   */
  ngOnInit() {
    this.page = parseInt(this.route.snapshot.queryParamMap.get('page'), 10) || 1;
    this.category = this.route.snapshot.paramMap.get('category') || 'all';

    this.subscriptions.push(combineLatest(
      this.route.params.pipe(pluck('category'), map(category => parseInt(category, 10) || 'all'), distinctUntilChanged()),
      this.route.queryParams.pipe(pluck('page'), map(page => parseInt(page, 10) || 1), distinctUntilChanged())
    ).subscribe(([category, page]) => {
      // когда категория меняется или запрашивается не загруженная ранее страница - очищаем список и рендерим заново
      if (this.category !== category || !this.isPageAlreadyLoaded(page)) {
        this.loadedPostsLists = [];
      }

      this.category = category;
      this.page = page;

      if (this.loadedPostsLists.length === 0) {
        this.fetchPosts(this.category, this.page);
      }
    }));
  }

  /**
   * Определяет, есть ли еще незагруженные страницы
   */
  get hasRemainingPosts() {
    const lastLoadedPage = this.loadedPostsLists.slice(-1)[0];
    return lastLoadedPage ? !lastLoadedPage.isLast : true;
  }

  /**
   * Определяет, была ли уже загружена страница
   */
  isPageAlreadyLoaded(page: number): boolean {
    return this.loadedPostsLists.some(val => val.page === page);
  }

  /**
   * Подгружает новые посты в `this.loadedPostsLists` в зависимости от переданной категории и страницы
   */
  fetchPosts(category, page) {
    let observable;
    this.isBusy = true;

    if (category === 'all') {
      observable = this.dataService.fetchPostsByPage(page);
    } else {
      observable = this.dataService.fetchPostsByCategoryAndPage(category as number, page);
    }

    this.subscriptions.push(observable.subscribe(postsList => {
      this.isBusy = false;

      if (postsList.posts.length) {
        this.loadedPostsLists.push(postsList);
      }
    }));
  }

  /**
   * Догружает посты на страницу
   */
  preloadNewPage() {
    if (this.loadedPostsLists.length && this.isBusy === false && this.hasRemainingPosts) {
      const lastPageList = this.loadedPostsLists.slice(-1)[0];
      this.fetchPosts(this.category, lastPageList ? lastPageList.page + 1 : this.page + 1);
    }
  }

  /**
   * Обновляет URL страницы при скроллинге между секциями
   */
  updatePageUrl(page) {
    const extras: NavigationExtras = { relativeTo: this.route };

    if (page > 1) {
      extras.queryParams = { page };
    }

    this.router.navigate(['.'], extras);
  }

  /**
   * Отписываемся от всех событий при уничтожении компонента
   */
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
