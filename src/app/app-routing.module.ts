import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostsListComponent } from './posts-list/posts-list.component';
import { PostDetailsModalComponent } from './post-details-modal/post-details-modal.component';


const routes: Routes = [
  { path: '', redirectTo: 'posts/all', pathMatch: 'full' },
  { path: 'posts/:category', component: PostsListComponent },
  { path: 'post-details/:post', outlet: 'modal', component: PostDetailsModalComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
