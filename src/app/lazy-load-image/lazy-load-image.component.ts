import { Component, ElementRef, Input, ViewChild } from '@angular/core';

@Component({
  selector: 'app-lazy-load-image',
  templateUrl: './lazy-load-image.component.html',
  styleUrls: ['./lazy-load-image.component.scss']
})
export class LazyLoadImageComponent {
  /**
   * Состояние картинки (загружена или нет)
   */
  isImageLoaded = false;

  /**
   * Адрес картинки
   */
  @Input() imageSrc: string;

  /**
   * Ссылка на img тег картинки поста
   */
  @ViewChild('image', { static: false }) image: ElementRef;

  /**
   * Начинаем загрузку изображения (динамически, при скролле до компонента)
   */
  startLoadingImage() {
    this.image.nativeElement.src = this.imageSrc;
  }
}
