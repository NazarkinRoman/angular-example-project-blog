import { Component, ElementRef, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { SinglePost } from '../typings';
import { FavoritesStorageService } from '../favorites-storage.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.scss']
})
export class SinglePostComponent implements OnInit, OnDestroy {
  /**
   * Находится ли пост в избранном
   */
  isAddedToFavorites = false;

  /**
   * Массив подписок на Observables
   */
  private subscriptions: Subscription[] = [];

  /**
   * Пост, который нужно отобразить
   */
  @Input() post: SinglePost;

  /**
   * Конструктор
   */
  constructor(private router: Router, private favoritePosts: FavoritesStorageService) {
  }

  /**
   * Подписываемся на изменения в списке избранного
   */
  ngOnInit(): void {
    this.subscriptions.push(this.favoritePosts.list().subscribe(
      favorites => this.isAddedToFavorites = favorites.some(favId => this.post.id === favId)
    ));
  }

  /**
   * Добавляет/удаляет пост в/из избранного
   */
  toggleFavorites() {
    if (this.isAddedToFavorites) {
      this.favoritePosts.remove(this.post.id);
    } else {
      this.favoritePosts.add(this.post.id);
    }
  }

  /**
   * Открывает модальное окно с полным текстом статьи
   */
  openPostModal() {
    this.router.navigate(
      [{ outlets: { modal: ['post-details', this.post.id] } }],
      { queryParamsHandling: 'preserve' }
    );
  }

  /**
   * Отписываемся от всех событий при уничтожении компонента
   */
  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
