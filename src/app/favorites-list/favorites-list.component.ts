import { Component, OnDestroy, OnInit } from '@angular/core';
import { FavoritesStorageService } from '../favorites-storage.service';
import { combineLatest, Subscription } from 'rxjs';
import { DataProviderService } from '../data-provider.service';
import { SinglePost } from '../typings';
import { toArray } from 'rxjs/operators';
import { animate, style, transition, trigger } from '@angular/animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-favorites-list',
  templateUrl: './favorites-list.component.html',
  styleUrls: ['./favorites-list.component.scss'],
  animations: [
    trigger('parentReveal', [
      transition(':enter', [])
    ]),
    trigger('childReveal', [
      transition(':enter', [
        style({ opacity: 0 }), animate('300ms ease-out')
      ]),
      transition(':leave', [
        animate('300ms ease-out', style({ opacity: 0 }))
      ])
    ])
  ]
})

export class FavoritesListComponent implements OnInit, OnDestroy {
  /**
   * Массив подписок на Observables
   */
  private subscriptions: Subscription[] = [];

  /**
   * Массив, содержащий объекты избранных постов
   */
  favorites: SinglePost[] = [];

  /**
   * Конструктор
   */
  constructor(
    private router: Router,
    private favoritePostsService: FavoritesStorageService,
    private dataService: DataProviderService) {
  }

  /**
   * Подписываемся на изменения в списке избранного
   */
  ngOnInit(): void {
    const favoritesList$ = this.favoritePostsService.list();
    const allPosts$ = this.dataService.fetchAllPosts().pipe(toArray());

    this.subscriptions.push(combineLatest(favoritesList$, allPosts$).subscribe(([favoritesList, allPosts]) => {
      const favorites = [];
      favoritesList.forEach(favId => {
        favorites.push(allPosts.find(post => post.id === favId));
      });
      this.favorites = favorites.reverse();
    }));
  }

  /**
   * Удаляет пост из избранного
   */
  removeFromFavorites(postId: number): void {
    this.favoritePostsService.remove(postId);
  }

  /**
   * Открывает модальное окно с полным текстом статьи
   */
  openPostModal(e: Event, postId: number): void {
    e.preventDefault();

    this.router.navigate(
      [{ outlets: { modal: ['post-details', postId] } }],
      { queryParamsHandling: 'preserve' }
    );
  }

  /**
   * Отписываемся от всех событий при уничтожении компонента
   */
  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
