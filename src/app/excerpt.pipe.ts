import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'excerpt' })
export class ExcerptPipe implements PipeTransform {
  /**
   * Генерирует выдержку определенной длины из заданного текста
   */
  transform(value: string, maxLength?: number, ellipsis?: string): string {
    maxLength = isNaN(maxLength) ? 150 : maxLength;
    ellipsis = ellipsis ? ellipsis : '...';

    const pureText = this.stripHtml(value);
    const originalWords = pureText.split(/\s+/);
    const acceptedWords = [];

    for (const currentWord of originalWords) {
      if (this.summaryLength(acceptedWords) + currentWord.length <= maxLength) {
        acceptedWords.push(currentWord);
      } else {
        acceptedWords.push(currentWord.replace(/\W+$/u, ''));
        return acceptedWords.join(' ') + ellipsis;
      }
    }

    return pureText;
  }

  /**
   * Считает суммарную длину всех слов в массиве (также, прибавляет 1 пробел между каждым словом).
   */
  summaryLength(wordsArray: string[]): number {
    return wordsArray.reduce((sum, val) => sum + val.length, 0) + Math.max(wordsArray.length - 1, 0);
  }

  /**
   * Удаляет все HTML теги из строки. Работает только в браузере.
   */
  stripHtml(html: string): string {
    const doc = new DOMParser().parseFromString(html, 'text/html');
    return doc.body.textContent || '';
  }
}
