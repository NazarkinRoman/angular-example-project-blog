import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';

@Directive({ selector: '[appInViewport]' })
export class InViewportDirective implements AfterViewInit, OnDestroy {
  @Input() inViewportOptions: IntersectionObserverInit;
  @Output() inViewportChange = new EventEmitter<boolean>();

  observer: IntersectionObserver;
  inViewport = false;

  /**
   * Добавляем класс `in-viewport-true`, когда элемент виден пользователю
   */
  @HostBinding('class.in-viewport-true')
  get isInViewport(): boolean {
    return this.inViewport;
  }

  /**
   * Добавляем класс `in-viewport-false`, когда элемент НЕ виден пользователю
   */
  @HostBinding('class.in-viewport-false')
  get isNotInViewport(): boolean {
    return !this.inViewport;
  }

  /**
   * Конструктор
   */
  constructor(private el: ElementRef) {
  }

  /**
   * После рендеринга шаблона настраиваем Intersection Observer
   */
  ngAfterViewInit() {
    const IntersectionObserver = window.IntersectionObserver;

    this.observer = new IntersectionObserver(
      this.intersectionObserverCallback.bind(this),
      this.inViewportOptions
    );

    this.observer.observe(this.el.nativeElement);
  }

  /**
   * Освобождаем ресурсы
   */
  ngOnDestroy() {
    if (this.observer) {
      this.observer.unobserve(this.el.nativeElement);
    }
  }

  /**
   * Колбэк Intersection Observer'a
   */
  intersectionObserverCallback(entries: IntersectionObserverEntry[]) {
    entries.forEach(entry => {
      if (this.inViewport === entry.isIntersecting) {
        return;
      }

      this.inViewport = entry.isIntersecting;
      this.inViewportChange.emit(this.inViewport);
    });
  }
}
