import { Component, ElementRef, HostListener, OnDestroy, OnInit } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { SinglePost } from '../typings';
import { combineLatest, Subscription } from 'rxjs';
import { DataProviderService } from '../data-provider.service';
import { filter } from 'rxjs/operators';
import { FavoritesStorageService } from '../favorites-storage.service';

@Component({
  selector: 'app-post-details-modal',
  templateUrl: './post-details-modal.component.html',
  styleUrls: ['./post-details-modal.component.scss'],
  animations: [
    trigger('modalReveal', [
      transition(':enter', [
        style({ opacity: 0, transform: 'translateY(-50px)' }), animate('300ms ease-out')
      ]),
      transition(':leave', [
        animate('300ms ease-out', style({ opacity: 0, transform: 'translateY(-50px)' }))
      ])
    ]),
    trigger('backdropReveal', [
      transition(':enter', [
        style({ opacity: 0 }), animate('300ms ease-out')
      ]),
      transition(':leave', [
        animate('300ms ease-out', style({ opacity: 0 }))
      ])
    ]),
  ]
})
export class PostDetailsModalComponent implements OnInit, OnDestroy {
  /**
   * Находится ли пост в избранном
   */
  isAddedToFavorites = false;

  /**
   * Массив подписок на Observables
   */
  private subscriptions: Subscription[] = [];

  /**
   * Объект поста, который нужно отобразить
   */
  post: SinglePost;

  /**
   * Конструктор
   */
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private favoritePosts: FavoritesStorageService,
    private element: ElementRef,
    private dataProvider: DataProviderService) {
  }

  /**
   * Переносим контент модального окна в конец <body>, чтобы оно отображалось поверх остальных окон
   */
  ngOnInit() {
    const queriedPostId = parseInt(this.route.snapshot.paramMap.get('post'), 10);
    const fetchQueriedPost = this.dataProvider.fetchAllPosts().pipe(filter(post => post.id === queriedPostId));

    document.body.appendChild(this.element.nativeElement);
    this.subscriptions.push(fetchQueriedPost.subscribe((post) => this.post = post));
    this.subscriptions.push(combineLatest(fetchQueriedPost, this.favoritePosts.list()).subscribe(
      ([queriedPost, favorites]) => this.isAddedToFavorites = favorites.some(favId => queriedPost.id === favId)
    ));
  }

  /**
   * Добавляет/удаляет пост в/из избранного
   */
  toggleFavorites() {
    if (this.isAddedToFavorites) {
      this.favoritePosts.remove(this.post.id);
    } else {
      this.favoritePosts.add(this.post.id);
    }
  }

  /**
   * Закрывает модальное окно
   */
  closeModal() {
    this.router.navigate(['.', { outlets: { modal: null } }], { queryParamsHandling: 'preserve' });
  }

  /**
   * Закрытие модального окна по нажатию на Escape
   */
  @HostListener('window:keyup', ['$event.keyCode'])
  closeOnKeyup(keyCode) {
    if (keyCode === 27) { // escape pressed
      this.closeModal();
    }
  }

  /**
   * Отписываемся от всех событий при уничтожении компонента
   */
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
    this.element.nativeElement.remove();
  }
}
